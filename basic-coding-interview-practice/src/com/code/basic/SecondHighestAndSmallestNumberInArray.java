package com.code.basic;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class SecondHighestAndSmallestNumberInArray {
    public static void main(String[] args) {
        Integer[] array = {11, -2, 83, 83, -4, 1, 0, -4, 11, 0};
        Set<Integer> uniqueAndSortedElements = new TreeSet<>(Arrays.asList(array));
        if (uniqueAndSortedElements.size() < 2) {
            System.out.println("Second higest element does not exist");
        } else {
            // int secondLargest = (int) uniqueAndSortedElements.toArray()[uniqueAndSortedElements.size() - 2];
            Object[] arr = uniqueAndSortedElements.toArray();
            System.out.println("Second Largest number in array: " + arr[arr.length - 2]);
            System.out.println("second Smallest number in array:" + arr[1]);
        }
        //Second way:
        System.out.println("Traditional way:");
        if (array.length < 2) System.out.println("there is not 2nd largest element");
        else {
            int largest = Integer.MIN_VALUE;
            int secondLargest = Integer.MIN_VALUE;
            for (Integer currentElement : array) {
                if (currentElement > largest) {
                    secondLargest = largest;
                    largest = currentElement;
                } else if (currentElement > secondLargest && currentElement != largest) {
                    secondLargest = currentElement;
                }
            }
            System.out.println("second largest number in array:" + array[array.length - 2]);
            System.out.println("second smallest number in array:" + array[1]);
        }
    }
}

