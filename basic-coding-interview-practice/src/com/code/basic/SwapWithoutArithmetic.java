package com.code.basic;

public class SwapWithoutArithmetic {
    public static void main(String[] args) {
        int number1 = 5;
        int number2 = 3;
            number1=number1^number2;    //5^3=2
            number2=number1^number2;    //2^5=3
            number1=number1^number2;    //2^3=5
        System.out.println("number1 = " + number1 + " and number2= " + number2);
    }

}
