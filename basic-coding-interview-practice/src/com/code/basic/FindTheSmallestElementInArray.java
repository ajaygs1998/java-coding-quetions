package com.code.basic;

public class FindTheSmallestElementInArray {
    public static void main(String[] args) {
        int[] array = {1, 4, -2, 4, 12, 5};
        int min = array[0];
        for (int currentElement : array) if (currentElement < min) min = currentElement;
        System.out.println("min element in array is: " + min);
    }
}
