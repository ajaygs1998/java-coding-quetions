package com.code.basic.string;

public class ReverseAString {
    public static void main(String[] args) {
        String name = "Ajay Ganesh Suryawanshi";
        StringBuilder newStr = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            newStr.append(name.charAt(name.length() - 1 - i));
        }
        System.out.println("ReversedString: " + newStr);
    }
}
