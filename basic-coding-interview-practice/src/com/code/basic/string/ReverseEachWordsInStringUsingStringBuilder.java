package com.code.basic.string;

public class ReverseEachWordsInStringUsingStringBuilder {
    public static void main(String[] args) {
        String myString = "go to school";
        String[] splitWords = myString.split(" ");
        StringBuilder reversedString = new StringBuilder();
        for (String str : splitWords) {
            StringBuilder reversedWord = new StringBuilder(str).reverse();
            reversedString = reversedString.append(reversedWord.append(" "));
        }
        System.out.println(reversedString.toString().trim());
    }
}
