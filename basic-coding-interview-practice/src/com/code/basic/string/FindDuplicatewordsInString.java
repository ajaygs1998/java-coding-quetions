package com.code.basic.string;

import java.util.HashSet;
import java.util.Set;

public class FindDuplicatewordsInString {
    public static void main(String[] args) {

        String myString = "I want to want to learn kafka and aws aws";
        String[] words = myString.toLowerCase().split(" ");
        Set<String> uniqueWords = new HashSet<>();
        Set<String> duplicateWords = new HashSet<>();
        for (String str : words) {
            if (!uniqueWords.add(str)) {
                duplicateWords.add(str);
            }
        }
        System.out.println(" duplicate words in the string are: " + duplicateWords);
    }
}
