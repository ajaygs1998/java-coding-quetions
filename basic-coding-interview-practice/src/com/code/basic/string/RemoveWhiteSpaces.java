package com.code.basic.string;

public class RemoveWhiteSpaces {
    public static void main(String[] args) {
        String name = "Ajay Ganesh Suryawanshi";
        StringBuilder newStr = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) != ' ') newStr.append(name.charAt(i));
        }
        System.out.println("Removed white spaces: " + newStr);
    }
}
