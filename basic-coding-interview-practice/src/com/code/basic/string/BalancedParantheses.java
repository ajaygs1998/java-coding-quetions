package com.code.basic.string;

public class BalancedParantheses {
    public static void main(String[] args) {
        String testString = "irtio";
        System.out.print("The given string is: ");
        System.out.print((checkBalanced(testString)) ? "balanced" : "unbalanced");
    }

    private static boolean checkBalanced(String testString) {
        int count = 0;
        for (int i = 0; i < testString.length(); i++) {
            Character ch = testString.charAt(i);
            if (ch == '(') {
                count++;
            } else if (ch == ')') {
                if (count == 0) {
                    return false;
                } else {
                    count--;
                }
            }
        }
        return count == 0;
    }
}
