package com.code.basic.string;

import java.util.Arrays;

public class SubSetsOfString {
    public static void main(String[] args) {
        String name = "Suryawanshi";
        // all possible subsets is : n(n+1)/2
        String[] newStr = new String[name.length() * (name.length() + 1) / 2];
        int temp = 0;
        for (int i = 0; i < name.length(); i++) {
            for (int j = i + 1; j <= name.length(); j++) {
                newStr[temp] = name.substring(i, j);
                temp++;
            }
        }
        System.out.println("Possible sustrings:  " + Arrays.toString(newStr));
    }
}