package com.code.basic.string;

public class TotalNumberOfStringInjava {
    public static void main(String[] args) {
        String name = "ajay surayawanshi";
        int count = 0;
        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) != ' ') {
                count++;
            }
        }
        System.out.println("Number of characters in string are: " + count);
    }
}
