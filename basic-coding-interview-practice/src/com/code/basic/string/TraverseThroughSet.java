package com.code.basic.string;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TraverseThroughSet {
    public static void main(String[] args) {
        Set<Integer> set = new HashSet<>();
        set.add(6);
        set.add(45);
        set.add(44);
        set.add(123);
        Iterator<Integer> mySet = set.iterator();
        while (mySet.hasNext()) System.out.println(mySet.next());
    }
}
