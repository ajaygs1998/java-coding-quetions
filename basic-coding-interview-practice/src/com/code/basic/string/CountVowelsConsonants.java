package com.code.basic.string;

public class CountVowelsConsonants {
    public static void main(String[] args) {
        int vCount = 0;
        int cCount = 0;
        String name = "Today is a monday";
        final var lowerCase = name.toLowerCase();
        for (int i = 0; i < name.length(); i++) {
            if (lowerCase.charAt(i) == 'a' || lowerCase.charAt(i) == 'e' || lowerCase.charAt(i) == 'i' || lowerCase.charAt(i) == 'o' || lowerCase.charAt(i) == 'u') {
                vCount++;
            } else if (lowerCase.charAt(i) > 'a' && lowerCase.charAt(i) <= 'z') {
                cCount++;
            }
        }
        System.out.println("count of Vowels: " + vCount + ", count of consonants: " + cCount);
    }
}
