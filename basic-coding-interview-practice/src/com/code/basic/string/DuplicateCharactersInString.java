package com.code.basic.string;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class DuplicateCharactersInString {
    public static void main(String[] args) {
        String duplicateStr = "Ajay Ganesh Suryawanshi";
        Set<Character> dupplicateChars = new HashSet<>();
        Set<Character> uniqueChars = new HashSet<>();
        for (Character ch : duplicateStr.toLowerCase().toCharArray()) {
            if (!uniqueChars.add(ch)) {
                dupplicateChars.add(ch);
            }
        }

        System.out.println("duplicate charaters in String are: ");
        Iterator duplicates = dupplicateChars.iterator();
        while (duplicates.hasNext()) {
            System.out.print(duplicates.next() + ", ");
        }

    }
}
