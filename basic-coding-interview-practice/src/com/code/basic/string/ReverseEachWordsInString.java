package com.code.basic.string;

public class ReverseEachWordsInString {
    public static void main(String[] args) {
        String myString = "go to school";
        String[] splitedwords = myString.split(" ");
        String reversedString = "";

        for (int i = 0; i < splitedwords.length; i++) {
            String singleWord = splitedwords[i];
            String reversedWords = "";
            for (int j = 0; j < singleWord.length(); j++) {
                reversedWords = reversedWords + singleWord.charAt(singleWord.length() - 1 - j);
            }
            reversedString = reversedString + reversedWords + " ";
        }
        System.out.println(reversedString);
    }
}
