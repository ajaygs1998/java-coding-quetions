package com.code.basic.string;

import java.util.Arrays;

/**
 * two strings are said to be anagrams if they contain same characters irrespective of order
 */
public class StringAnagram {
    public static void main(String[] args) {
        String a = "Brag";
        String b = "Grab";
        System.out.println(anagramChecker(a, b) ? "String a and b are anagrams" : "string a and b are not anagrams");
    }

    private static boolean anagramChecker(String a, String b) {
        if (a.equals(b)) return true;
        if (a == null || b == null) return false;
        if (a.length() == b.length()) {
            char[] aCharArray = a.toLowerCase().toCharArray();
            char[] bCharArray = b.toLowerCase().toCharArray();
            Arrays.sort(aCharArray);
            Arrays.sort(bCharArray);
            return Arrays.equals(aCharArray, bCharArray);
        }
        return false;
    }
}
