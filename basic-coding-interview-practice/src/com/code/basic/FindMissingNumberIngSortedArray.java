package com.code.basic;

public class FindMissingNumberIngSortedArray {
    public static void main(String[] args) {
        //99 is missing in array
        int[] numbers = {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
                41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
                51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
                61, 62, 63, 64, 65, 66, 67, 68, 69, 70,
                71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
                81, 82, 83, 84, 85, 86, 87, 88, 89, 90,
                91, 92, 93, 94, 95, 96, 97, 98, 100
        };
        int expectedSizeOfArray = numbers.length + 1;
        //formula to find the sum of numbers in series: sum=n(n+1)/2
        int expectedSum = (expectedSizeOfArray) * (expectedSizeOfArray + 1) / 2;
        int actualSum = 0;
        for (Integer element : numbers) actualSum += element;
        System.out.println("missing number is: " + (expectedSum - actualSum));
        //second approach
        System.out.println("second approach:");
        int xorArrElements = 0;
        int xorTotal = 0;
        for (Integer e : numbers) {
            xorArrElements ^= e;
        }
        for (int i = 1; i <= 100; i++) {
            xorTotal ^= i;
        }
        int missingNumber = xorTotal ^ xorArrElements;
        System.out.println("missing number using xor: " + missingNumber);
    }
}
