package com.code.basic;

import java.util.Arrays;

public class ReversingTheArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 5};
        int start = 0;
        int end = array.length - 1;
        while (end > start) {
            int temp = array[end];
            array[end] = array[start];
            array[start] = temp;
            end--;
            start++;
        }
        System.out.println("Reversed Array: " + Arrays.toString(array));

    }
}
