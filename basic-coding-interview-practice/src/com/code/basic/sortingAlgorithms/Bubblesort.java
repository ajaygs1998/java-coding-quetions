package com.code.basic.sortingAlgorithms;

import java.util.Arrays;

/**
 * Bubble sort is like arranging numbers in a line from smallest to largest by repeatedly
 * checking pairs of adjacent numbers and swapping them if they're in the wrong <u>order</u>.
 */
public class Bubblesort {
    public static void main(String[] args) {
        int[] numbers = {5, 2, 9, 3, 4}; // Example array of numbers

        // Outer loop to go through the array
        for (int i = 0; i < numbers.length - 1; i++) {
            // Inner loop to compare and swap adjacent numbers
            for (int j = 0; j < numbers.length - 1-i; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }
        System.out.printf("BubbleSort: "+ Arrays.toString(numbers));
    }
}
