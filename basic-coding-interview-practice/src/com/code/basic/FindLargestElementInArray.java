package com.code.basic;

public class FindLargestElementInArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 5};
        int max = array[0];
        for (int element : array) {
            if (element > max) {
                max = element;
            }
        }
        System.out.println("max element in array is: " + max);
    }
}
