package com.code.basic;

import java.util.Scanner;

/**
 * what is the amstrong number?
 * A positive number is said to be Armstrong number the sum of the digits cube is equal to the number.
 */
public class AmstrongNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter the number to check Armstrong:");
        int number = scanner.nextInt();
        if (number < 0) {
            System.out.println("number should not in negative");
        } else if (number == 0 || number == 1) {
            System.out.println("The given number " + number + " is a Armstrong number");
        }
        int sum = 0;
        final int originalNumber = number;
        while (number > 0) {
            int digit = number % 10;   //extract the digits
            sum += Math.pow(digit, 3);
            number /= 10;      // reduce the number size
        }
        if (sum == originalNumber) {
            System.out.println("The given number " + originalNumber + " is a Armstrong number");
        } else {
            System.out.println("The given number " + originalNumber + " is not a Armstrong number");
        }
    }
}
