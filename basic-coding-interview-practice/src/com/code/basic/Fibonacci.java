package com.code.basic;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter the number of terms in Fibonacci series: ");
        int termCount = scanner.nextInt();
        if (termCount <= 0) {
            System.out.println("please enter the positive number");
            return;
        } else {
            int firstTerm = 0, secondTerm = 1;
            for (int i = 1; i <= termCount; i++) {
                System.out.print(firstTerm + ", ");
                int nextTerm = firstTerm + secondTerm;
                firstTerm = secondTerm;
                secondTerm = nextTerm;
            }
        }
    }
}

