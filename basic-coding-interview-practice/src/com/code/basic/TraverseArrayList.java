package com.code.basic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TraverseArrayList {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(2);
        list.add(4);
        list.add(8);
        list.add(19);
        System.out.println("Traverse through for loop");
//        for(Integer i: list){
//            System.out.println(i);
//        }

        Iterator myList = list.listIterator();
        while (myList.hasNext()) {
            System.out.println(myList.next());
        }
    }
}
