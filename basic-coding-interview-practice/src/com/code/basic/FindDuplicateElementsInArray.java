package com.code.basic;

import java.util.HashSet;
import java.util.Set;

/**
 * the Hashset contains the unique elements
 * add method returns: true if element inserted successfully and false if element is not added(means same element is already present)
 */
public class FindDuplicateElementsInArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 5};
        Set<Integer> uniqueElements = new HashSet<>();
        Set<Integer> duplicateElements = new HashSet<>();
        for (int j : array) {
            if (!uniqueElements.add(j)) {
                duplicateElements.add(j);
            }
        }
        System.out.println("Duplicate elements in the array are:" + duplicateElements);

    }
}
