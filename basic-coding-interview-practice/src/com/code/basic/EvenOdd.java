package com.code.basic;

public class EvenOdd {
    public static void main(String[] args) {
        int num = 19;
        System.out.println("The " + num + " is " + isEvenNumber(num) + " number");
    }

    private static String isEvenNumber(int num) {
        if (num % 2 == 0) {
            return "even";
        }
        return "odd";
    }
}
