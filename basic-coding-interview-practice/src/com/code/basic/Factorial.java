package com.code.basic;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        Double number = scanner.nextDouble();
        scanner.close();

        if (number < 0) {
            System.out.println("factorial of the negative number can't be caluclated");
        } else {
            System.out.println("Factorial of " + number + " is " + findFactorial(number));
        }

    }

    private static Double findFactorial(Double number) {
        if (number == 0 || number == 1) {
            return Double.valueOf(1);
        }
        Double factorial = Double.valueOf(1);
        for (var i = Double.valueOf(1); i <= number; i++) {
            factorial = factorial * i;
        }
        return factorial;
    }
}
