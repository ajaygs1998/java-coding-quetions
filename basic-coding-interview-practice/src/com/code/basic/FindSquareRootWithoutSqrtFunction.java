package com.code.basic;

import java.util.Scanner;

//for this use the newton-raphson method   xi=(xi+ n/xi)/2
public class FindSquareRootWithoutSqrtFunction {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number to find square-root: ");
        float number = scanner.nextFloat();
        double approximateRoot = number;// approximateRoot

        for (int i = 1; i <= 10; i++) {
            approximateRoot = (approximateRoot + number / approximateRoot) / 2;
        }
        System.out.println("sqrt of " + number + " is " + approximateRoot);
    }
}
