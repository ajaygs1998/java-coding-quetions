package com.code.basic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class OddAndEvenNumberInArray {
    public static void main(String[] args) {
        Integer[] array = {12, 12, 33, -34, 5, 5, 7, 0, -1};
        Set<Integer> uniqueElements = new HashSet<>(Arrays.asList(array));
        uniqueElements.forEach(n -> {
            if (n % 2 == 0) {
                System.out.println("Even: " + n);
            } else {
                System.out.println("Odd: " + n);
            }
        });
    }
}
